import os, re, glob
import numpy as np
from itertools import izip
from StringIO import StringIO

import pandas as pd
import table_IO
import itertools


from config import cfg
global cfg

from IPython import embed

# what are the dcol-headers in the gaze-data mean? 
_dcol_headers = dict(
    LR = ['time','L_gaze_x','L_gaze_y','L_pupil',
                 'R_gaze_x','R_gaze_y','R_pupil',
                 'L_vel_x','L_vel_y','R_vel_x','R_vel_y'],
    R = ['time','R_gaze_x','R_gaze_y','R_pupil','R_vel_x','R_vel_y'],
    L = ['time','L_gaze_x','L_gaze_y','L_pupil','L_vel_x','L_vel_y'],
)

class EDF2ASC(object):
    """Run the edf2asc progam -- twice"""
    def __init__(self, inpath, gzcommand=None, evtcommand=None):
        super(EDF2ASC, self).__init__()
        E2Acfg = cfg['eyelink']['edf2asc']
        # inpath = inpath if inpath is not None else E2Acfg['']
        self.gzcmd = (gzcommand if gzcommand is not None 
            else " ".join( [E2Acfg['basecommand'], E2Acfg['gzflags']]  ) )
        self.evcmd = (evtcommand if evtcommand is not None 
            else " ".join( [E2Acfg['basecommand'], E2Acfg['evtflags']] ) )
        return

    def run(self, subfolder, fpattern='*.edf'):
        self.run_folder = subfolder

        # find the file to process
        dfile = glob.glob(os.path.join(subfolder, fpattern))
        assert len(dfile) == 1 # if not, better pattern needed!
        dfile = dfile[0]

        # ouput/input for EDF2ASC
        ifile = dfile # edf-file
        self.stem = stem = os.path.splitext(dfile)[0]
        afile = stem + '.asc' # intermediate asc file
        ofile = stem + '.evt' # desired outfile
        
        # run the event command (s)
        if self.evcmd != '':
            evcmd = self.evcmd
            cmd = "{cmd} {outpath} {inf}".format(
                cmd=evcmd, inf=ifile, outpath=subfolder)
            os.system( cmd )
            os.system( "mv {} {}".format(afile, ofile) )

        # run the sample-commands 
        if self.gzcmd != '':
            gzcmd = self.gzcmd
            ofile= stem + '.gaz'
            cmd = "{cmd} {outpath} {inf}".format(
                cmd=gzcmd, inf=ifile, outpath=subfolder)
            os.system( cmd )
            os.system( "mv {} {}".format(afile, ofile) )
            self._gzclean(ofile)
            self._gznp(ofile)
        return
    
    def _gzclean(self,fname,write=False):
        print 'gzclean'
        with open(fname) as gf:
            self.gstr = gf.read()
        self.gstr = re.sub(re.compile('[A-Z]+'), '', self.gstr)
        self.gstr = re.sub(re.compile('\t+\.+'), '', self.gstr)
        return
    
    def _gznp(self,fname):
        print("Saving gaze data")
        dummyfile = StringIO(self.gstr)
        gzdatnp = np.genfromtxt(dummyfile)
        gzdatnp[gzdatnp==0.0001] = np.nan # nans are nans
        print("Does the following take too long? Then don't do it!")
        np.save(fname, gzdatnp)
        # keep the .gaz.npy, remove the .gaz
        os.system('rm {}'.format(fname))
        return

class EDFParser(object):
    """Parses edfs into an awesome h5 table"""
    def __init__(self, events = 'none'):
        super(EDFParser, self).__init__()
        pcfg = cfg['eyelink']['parsing']
        self.events = pcfg['events']
        self.samples= pcfg['samples']
        self.variables=pcfg['variables']
        self.standardize_coord=pcfg['standard_coordinates']
        try:
            self.custom_events = pcfg['custom_events']
        except KeyError, e:
            self.custom_events = []

        self.evt_str = ''
        self.gz_str = ''
        # TODO future: **kwargs to pass on to EDF2ASC when necessary?
        self.output_file = 'eyelink.h5'
        return

    def e2acheck(self, subfolder, gzcmd=None, evtcmd=None, 
                 evpattern='*.evt', gzpattern='*.gaz.npy'):
        
        # patterns
        evpattern = os.path.join( subfolder, evpattern )
        gzpattern = os.path.join( subfolder, gzpattern )

        # if samples don't have to be parsed or the gaz.npy file already exists
        if (self.samples == False or len(glob.glob(gzpattern)) > 0 ):
            # no new gaze parsing needed:
            gzcmd = ''
        # Same for events:
        if ((not self.events and not self.custom_events) or
            len(glob.glob(evpattern)) > 0):
            evtcmd = ''
        # is there anything to left to parse
        if evtcmd == '' and gzcmd == '':
            return
        # if there is,do either with None (deflt) One of them blank, or user cmd
        e = EDF2ASC(subfolder, gzcmd, evtcmd)
        e.run(subfolder)
        return

    def parse_recording_parameters(self):
        """
        Parses resolution and sample rate; from evt file
        """
        # patterns:
        # fetch sample rate and recorded eye
        srate_eye_patt = 'MSG\s[\d\.]+\s!MODE RECORD CR (\d+) \d+ \d+ (\S+)'
        # fetch screen_resolution:
        res_patt = ('MSG\s[\d\.]+\sGAZE_COORDS ' + 
                     '(\d+.\d+)\s(\d+.\d+)\s(\d+.\d+)\s(\d+.\d+)')

        #### get sample rate and eye recorded from the evt str
        srate_eye = re.findall(re.compile( srate_eye_patt ), self.evt_str)
        coords = re.findall(re.compile( res_patt ), self.evt_str)
        
        
        # concat to pandas dataframe
        self.rec_params = pd.concat(
            (pd.DataFrame(srate_eye), pd.DataFrame(coords)), axis=1)
        self.rec_params.columns = ['sample_rate','eye', 
            'screen_xmin','screen_ymin','screen_xmax','screen_ymax']

        return

    def parse_fixations(self):
        # patterns (every line starting with EFIX)
        fixpatt = 'EFIX (.+)'
        fixlist = re.findall(re.compile(fixpatt), self.evt_str)
        fixvals = [ [float(v) for v in 
                        re.findall(re.compile('(\d+[\.\d+]+)'), f )] 
                    for f in fixlist]
        # turn this into a dataframe:
        fixkeys = ['start_tstamp','end_tstamp','duration', 'x','y','pupil_size']
        self.fixations = pd.DataFrame(fixvals, columns = fixkeys)

        return

    def parse_saccades(self):
        # patterns (every line starting with ESACC)
        def _correct_missing_val_saccs(val_row):
            assert len(val_row) == 10 # fix only works for the following bug
            
            for i,v in enumerate( val_row[:-3] ): # values -2 and -3 are nonsense
                if v < .001:                      # EL missing value format
                    val_row[i] = np.nan
            # fix last 2 values:
            val_row.pop(-3)
            val_row[-2] = np.nan
            return val_row

        sacpatt = 'ESACC (.+)'
        saclist = re.findall(re.compile(sacpatt), self.evt_str)
        sacvals = [ [float(v) for v in 
                        re.findall(re.compile('(\d+[\.\d+]+)'), f )] 
                    for f in saclist]

        # EL missing vals are turned into 0.0001, which causes peak_vel = 3e+006
        # this is then turned into 3.0 and 6.0, which is wrong. This is fixed here:
        sacvals = [_correct_missing_val_saccs(row) 
                        if len(row) ==10 else row for row in sacvals]
        # turn this into a dataframe:
        sackeys = ['start_tstamp','end_tstamp','duration',
            'start_x','start_y','end_x','end_y', 'peak_velocity', 'amplitude']
        self.saccades = pd.DataFrame(sacvals, columns = sackeys)

        return

    def parse_blinks(self):
        # patterns (every line starting with EBLINK)
        blkpatt = 'EBLINK (.+)'
        blklist = re.findall(re.compile(blkpatt), self.evt_str)
        blkvals = [ [float(v) for v in 
                        re.findall(re.compile('(\d+[\.\d+]+)'), f )] 
                    for f in blklist]
        # turn this into a dataframe:
        blkkeys = ['start_tstamp','end_tstamp','duration']
        self.blinks = pd.DataFrame(blkvals, columns = blkkeys)
        
        return

    def parse_variables(self):
        # Pull apart all TRIAL_VAR lines:
        varpatt = 'MSG\s+(\d+)\s\!V\sTRIAL_VAR\s(\S+)\s(\S+)'
        varlist = re.findall(re.compile(varpatt), self.evt_str)
        if not varlist:
            print "W: no variables (!V) found in event file"
            return

        # group the rows by tstamp, (one group if tstamps are within 50ms)
        tstamps = [int(t) for t,var,val in varlist]
        indices = np.r_[0, (np.diff(tstamps) > 50).cumsum()]

        # 'replace' tstamp by an index value:
        idx_varlist = [(ind, var, val) for (ind, (tstamp, var,val)) in zip(indices, varlist)] 

        # now group by these indices to create a list of dictionaries per trial:
        all_trs = []
        for ind, vals in itertools.groupby(idx_varlist, lambda x:x[0]):
            d = dict((y,z) for x,y,z in vals)
            d.update(dict(tstamp=tstamp))
            all_trs += [d]
        # turn this into a pandas DF:
        self.variables = pd.DataFrame(all_trs)
                
        return

    def parse_custom_events(self):
        seg_strings = re.findall('START(.+?)END', self.evt_str, re.DOTALL)
        if not seg_strings:
            print "W: no recording blocks found in event file"
            return

        nonmatch_warnings = []
        for i, segstr in enumerate(seg_strings):
            # row = pd.DataFrame(idx=i)
            for evt in self.custom_events:
                keys = evt['key']
                patt = re.compile(evt['pattern'])
                # look for the event in every segment
                matches = re.findall(patt, segstr)
                # if no or not enough matches found, make them nans because 
                # we'll never know what value was the correct one
                if not matches:
                    matches = [(np.nan,) * len(keys)]
                    nonmatch_warnings += keys
                else:
                    # if there is only 1 value, it is returned as [x], 
                    # else as [(x,x,x)].  this line generalizes [x] to [(x,)]
                    if type(matches[0]) != tuple:
                        matches[0] = (matches[0],)
                    matches[0] = list(matches[0]) # make it mutable
                    # # now try to make each match a number:
                    # for i,m in enumerate(matches[0]):
                    #     try:
                    #         matches[0][i] = float(m) if '.' in m else int(m)
                    #     except ValueError, e:
                    #         pass
                # if there are LESS matches than keys: everything is a nan
                # (as we don't know which match corresponded to which key...)
                if len(matches[0]) < len(keys):
                    matches = [(np.nan,) * len(keys)]
                    nonmatch_warnings += keys
                # if there are MORE values being found than supposed to -- try to
                # fix it by labelling the keys; (i.e. 'cue' becomes cue_0, cue_1, cue_2....)
                if len(matches[0]) > len(keys):
                    if len(keys) == 1:
                        keys = ["{}_{}".format(keys[0], i) 
                                    for i in range(len(matches))]
                # now make it a row, and grow it outwards:
                try:
                    row = pd.concat(
                        (row, pd.DataFrame(matches, columns=keys)), axis=1)
                except NameError, e:
                    row = pd.DataFrame(matches, columns=keys)
            # at the end of every seg (event):
            row['idx'] = i
            try:
                df = pd.concat( (df, row) )
            except NameError, e:
                df = row
            del row
        try:
            if np.all(np.isnan(df.values.flatten())):
                print "W: No matches found at all for custom events: no table created "
                return            
        except TypeError, e: # some values don't allow isnan -- so that means there were values
            pass
                    
        # inform the user about nonmatches
        if nonmatch_warnings:
            nonmatch_warnings = list(set(nonmatch_warnings))
            print ("W: not every row had the expected nr of matches for keys {}"
                    .format(nonmatch_warnings) )

        # make every number a number in one go
        for c in df.columns:
            vv = df[c].values
            if vv.dtype == int or vv.dtype == float:
                continue
            if not ('.' in ''.join(df[c].values)):
                thetype = int
            else: 
                thetype = float
            try:
                df[c] = df[c].values.astype(thetype)
            except ValueError as e:
                pass

        self.cust_evts  = df

        return

    def parse_samples(self, gap=50, buff_interp=100):
        # identify segments:
        gaze_times = self.gz_data[:, 0]
        segm_idx = (np.diff(gaze_times) > gap).cumsum()
        segm_idx = np.r_[segm_idx, segm_idx[-1]] # and one more, for last idx

        # if we have a parsed event datafile, 
        # check and assert that it matches up; and use its eye-info
        try:
            # assert segments match number of 'start_recording' calls
            assert self.rec_params.shape[0] == np.unique(segm_idx).shape[0]
            eye = self.rec_params['eye'].iloc[0]
        except AttributeError:
            print "W: no event file parsed, so gaze sample header is only guessed"
            eye='R'

        self.gz_data = np.hstack((segm_idx[:,np.newaxis], self.gz_data))
        header = ['idx'] + _dcol_headers[eye]
        self.gzdf = pd.DataFrame(self.gz_data, columns=header)

        # Make timepoints with missing/erroneous data into NA;
        # wherever pupil oob, x oob or y oob: make sample NA
        err_rows = []
        checked_cols = []
        df = self.gzdf
        screen_xmax = float(self.rec_params.screen_xmax.values[0])
        screen_ymax = float(self.rec_params.screen_ymax.values[0])
        for c in df.columns:
            if not (('gaze_x' in c) or ('gaze_y' in c) or ('pupil' in c)):
                continue
            checked_cols.append(c)
            if 'pupil' in c:
                bads = np.where(df[c] < 1)[0]
            if 'gaze_x' in c:
                bads = np.where( (df[c] < 0) | (df[c] > screen_xmax) )[0]
            if 'gaze_y' in c:
                bads = np.where( (df[c] < 0) | (df[c] > screen_ymax) )[0]
            err_rows = np.r_[err_rows, bads]

        err_rows = np.unique(err_rows).astype(int)
        for c in checked_cols:
            self.gzdf[c].iloc[err_rows] = np.nan
        # if we have events, make area around blinks NA
        if hasattr(self,'blinks'):
            # pts around blinks should also be NA;
            blinktimes = self.blinks[['start_tstamp','end_tstamp']].values
            blinktimes[:,0]-=buff_interp
            blinktimes[:,1]+=buff_interp
            # A  = 'times > blink_onsets'
            A = self.gzdf.time.values[:,np.newaxis] > blinktimes[:,0][np.newaxis,:]
            # B  = 'times < blink_offset'
            B = self.gzdf.time.values[:,np.newaxis] < blinktimes[:,1][np.newaxis,:]
            inv_times = np.any(A&B, axis = 1)
            for c in checked_cols:
                self.gzdf[c].loc[inv_times] = np.nan

        # then, interpolate those NA's
        interp_df = self.gzdf.copy().set_index('time').interpolate(kind='linear').reset_index()
        for c in checked_cols:
            self.gzdf[c + '_i'] = interp_df[c]

        
        return

    def run(self, subfolder, evpattern='*.evt', gzpattern='*.gaz.npy'):
        # find the file(s) to process
        self.run_folder = subfolder
        self.subj= subfolder.split(os.path.sep)[-1]
        # if 'eyelink.h5':
        if os.path.isfile(os.path.join(self.run_folder,'eyelink.h5')):
            print 'skipping...output file already exists.'
            return 

        evpattern = os.path.join( subfolder, evpattern )
        gzpattern = os.path.join( subfolder, gzpattern )
        assert len(glob.glob(evpattern)) <= 1 # multiple matches!!
        assert len(glob.glob(gzpattern)) <= 1  # multiple matches!!

        # no matches?
        if not len(glob.glob(evpattern)) or not len(glob.glob(gzpattern)):
            ## Check whether e2a has to be run:
            self.e2acheck(subfolder)

        # read the text files (and parse them)
        if self.events or self.custom_events:
            evfile = glob.glob(evpattern)[0]
            with open(evfile,'r') as evtf:
                self.evt_str = evtf.read()
            self.parse_recording_parameters()
            # run the parsers for each event:
            for ev in self.events:
                eventfun = getattr(self,'parse_'+ ev)
                eventfun()

            # parse eyelink variables:
            if self.variables:
                self.parse_variables()

            # any custom events:
            self.parse_custom_events()

        ### samples:
        if self.samples:
            gzfile = glob.glob( gzpattern)[0]
            self.gz_data = np.load(gzfile)
            self.parse_samples()

        ### standardaze_coords?
        if self.standardize_coord:
            self.standard_coordinates()

        # save
        self.save()
        return        

    def standard_coordinates(self):
        """
        Opensesame 3 introdces the great concept of standard coordinates:
        0,0 is the center of the screen. Note: positive is down, though.
        Standard Eyelink behavior is 0,0 is top left, y positive is down.
        This function transforms standardized coordinates for samples and evts
        """
        # transformation functions:
        eye, xmax,ymax = self.rec_params.loc[0, ['eye','screen_xmax','screen_ymax']]
        xmax = float(xmax); ymax=float(ymax)
        transform = dict()
        transform['x'] = lambda x, xmax=xmax: x - xmax / 2.0
        transform['y'] = lambda y, ymax=ymax: y - ymax / 2.0

        # samples:
        if hasattr(self, 'gzdf'):
            for e, c in itertools.product(eye, 'xy'):
                field = e + '_gaze_' + c # e.g. 'L_gaze_x'
                # copy into raw:
                self.gzdf[field + '_raw'] = self.gzdf[field]
                self.gzdf[field] = transform[c]( self.gzdf[field] )
                try: 
                    field = field + '_i'
                    self.gzdf[field] = transform[c]( self.gzdf[field] )
                except KeyError:
                    pass

        # saccades
        if hasattr(self, 'saccades'):
            for phase, c in itertools.product(['start','end'], 'xy'):
                field = phase + '_' + c # e.g. 'start_x'
                # copy into raw:
                self.saccades[field + '_raw'] = self.saccades[field]
                self.saccades[field] = transform[c](self.saccades[field])


        # fixations
        if hasattr(self, 'fixations'):
            for c in 'xy':
                # copy into raw:
                self.fixations[c + '_raw'] = self.fixations[c]
                # transform
                self.fixations[c] = transform[c](self.fixations[c])

        return 

    def _save_helper(self, tabname, leafname):
        """
        helper function for save
        """
        try:
            tab = getattr(self, tabname)
            table_IO.write_pd_df(tab, 
                '/{}/{}'.format(self.subj, leafname), 
                fname=os.path.join(self.run_folder, self.output_file) )
        except AttributeError, e: # table was never made
            pass
        return

    def save(self):
        """
        Write all the created tables to the eyelink h5 file
        """
        # all tables
        tabnames = ['gzdf','rec_params','cust_evts','variables',
                    'saccades', 'fixations','blinks']
        # their names in the h5 file
        names    = ['gaze_samples', 'recording_params','custom_events','variables',
                    'saccades', 'fixations','blinks']
        # call the save helper:
        _ = [self._save_helper(*args) for args in zip(tabnames, names)]

        return


def main():
    dfolder = cfg['datafolder_pp']
    for d in sorted(glob.glob(dfolder + '/*')):
        print d, '...'
        p  = EDFParser(d)
        p.run(d)
        print 'done.'


        
if __name__ == '__main__':
    main()
    # sub_datafolder = 'data_pp/s01st'
    # p = EDFParser(sub_datafolder)
    # p.run(sub_datafolder)
    # embed()
    # run the parser