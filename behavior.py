import pandas as pd
import numpy as np 

import os, glob
import numpy as np
from itertools import izip

import rpy2.robjects as R
import pandas.rpy.common  as com

from IPython import embed

import table_IO

from config import cfg

class Behavior_R(object):
    """docstring for Behavior_R"""
    def __init__(self, script=None, hook=None):
        super(Behavior_R, self).__init__()
        assert 'R' in iter(cfg['behavioral']['choice'])
        # read from the cfg, or from the arg
        Rcfg = cfg['behavioral']['opts']['R']
        self.scriptfile = script if script is not None else Rcfg['file']
        self.hook = hook if hook is not None else Rcfg['pyhook']
        self.table = None
        return

    def run(self, subfolder, fpattern='*.csv'):
        """
        Run an R script to preprocess a 'relevant csv'
        """
        self.run_folder = subfolder
        self.subj= subfolder.split(os.path.sep)[-1]


        # find the file to process
        dfile = glob.glob(os.path.join(subfolder, fpattern))
        assert len(dfile) == 1 # if not, better pattern needed!
        dfile = dfile[0]

        # find the script
        script_name = self.scriptfile
        hook = self.hook

        ## interfacing with R:
        try:
            with open(script_name) as sf:
                script = sf.read()
            r_result = R.r(script)
            rpyhook = R.r[hook]
            # set filename (globally) in environment:
            R.r("fname <<- '{}'".format(dfile))
            # run the stuff:
            r_result = rpyhook()
        except IOError, e:
            print "Script [{0}] not found; skipped".format(script_name)
            raise e
        except ValueError, e:
            print "Error in script".format(script_name)
            raise e
        # Else, it's probably a 'lookuperror'; don't know where exeption defined
        except Exception, e: 
            print "Function {} not found".format(RFun)
            raise e
        # turn it into a pandas dataframe.
        table = com.convert_robj(r_result)
        table['origin'] = dfile
        table['group'] = self.subj
        self.table = table
        return

    def save(self, output_file='behavior.h5'):
        assert self.table is not None
        print "Saving", output_file, 'in', self.run_folder
        table_IO.write_pd_df(self.table, '/'+self.subj+'/behavior', 
            fname=os.path.join(self.run_folder, output_file) )
        return


class Behavior_py(object):
    """docstring for Behavior_py"""
    def __init__(sel, *args, **kwargs):
        raise NotImplementedError("python behav. processing is NI")
        pass
        
def main():
    for d in sorted(glob.glob('data_pp/s*')):
        print d, '...'
        b = Behavior_R()
        b.run(d)
        b.save()
        print 'done.'



if __name__ == '__main__':
    main()
    # sub_datafolder = 'data_pp/s01st'
    # b = Behavior_R()
    # b.run(sub_datafolder)
    # b.save()
