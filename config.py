import yaml

# default config file:
with open('cfg.yaml') as yml:
    cfg = yaml.load(yml)

# current config file:
with open('cfg_rep_or_lm.yaml') as yml:
    cfg = yaml.load(yml)