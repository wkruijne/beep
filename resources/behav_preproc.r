library(dplyr)
library(magrittr)

# returns numeric vector with 1 for repetition, 0 for nonrepetition, 
get_rep_vector <- function(vals){
    # and 2 for the first values
    A = vals
    B = vals[2:length(vals)]
    # the following will yield a Warning:
    f = function(A,B){A==B}
    X = as.numeric(suppressWarnings(f(A,B)))
    repvec = c(2, X[1:(length(X)-1)])
    f.repvec = factor(repvec, levels = c(0,1,2), labels=c('swi','rep','none'))
    return( f.repvec )
}

preproc <- function(dfname) {
	# Filter irrelevant cols
	dat <- read.csv(dfname)
	
	write("UPDATE YOUR R-SCRIPT, WOUTER!!!_CURRENT BIAS IS NOT LOGGED YET",'')
	dat  %<>% select(subject_nr, Block_nr, block_type, trial_idx,
					correct, response, response_time,
					target_color, target_resp, target_type,
					layout_index, layout_type,  disp_str, 
					jitt, 
					count_logger, count_parallel_port_trigger, 
					time_display, time_fixcue, 
					time_parallel_port_trigger , time_stop_recording)

	# No eyelink recording on practice -- eeg wasn't yet recording either
	dat  %<>% filter(block_type != 'practice')

	# mark repetitions:
	dat  %<>% group_by(Block_nr)  %>% 
		mutate(feat_rep = get_rep_vector(target_color),
			   resp_rep = get_rep_vector(target_resp))
	
	# assign bias_block_nr
	write("UPDATE YOUR R-SCRIPT, WOUTER!!! BIAS_BLOCK_NRING NOT UP TO DATE", '')
	# also -- replace this with more legible ifelse statement
	dat  %<>% group_by(block_type)  %>% 
		mutate(bias_block_nr=block_type  %>% {
				if(.[1] %in% c('memtest','practice')){
					return(0)
				} 
				# else, return:
				c(	rep(1, 100), 
					seq(2,5)  %>% rep(each=200) )		
			})
	# return as df (pandas prolly doesn't work nicely with dplyr)
	return(dat  %>% data.frame)
}


rpy_hook <- function(x) {
        stopifnot(exists('fname'))
    write(paste("R > Call preproc on", fname), '')
    tab <- preproc(fname)
    return(tab)
}