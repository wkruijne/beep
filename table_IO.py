import pandas as pd 
import tables as tb
from pandas import HDFStore
import os
from IPython import embed
import numpy as np

############### quick reader/writer (for pandas tables)

def write_pd_df(data, h5path='/', fname='table.h5', mode='a'):
    data.to_hdf(fname, key=h5path,mode=mode)
    return

def read_pd_df(fname, h5path):
    with pd.get_store(self.fname) as store:
        data = store[h5path]
    return data

def merge_subject_tables(fnames, outname='merged.h5'):
    # TODO: check whether it already exists, and warn user about removal
    for f in fnames:
        tab = HDFStore(f, mode='a')
        outhandle = tab.copy(outname, mode='a')
        outhandle.close()
        tab.close()
    return

##################


class EyeDataQuery(object):
    """class to interface with the generated HDF5"""
    def __init__(self, fname, group=None):
        super(EyeDataQuery, self).__init__()

        with pd.get_store(fname) as store:
            allkeys = store.keys()
            allgroups = list(set([ k.split('/')[1] for k in allkeys]))

        self.fname = fname
        self.keys = allkeys
        self.groups = ['/'+ g for g in allgroups]
        self.group= self.groups[0]
        return

    @property
    def group(self):
        return self._group
    
    @group.setter
    def group(self, value):
        if not value.startswith('/'):
            value = '/'+ value
        assert value in self.groups
        self._group = value
        return
    
    def get_table(self, table_name):
        with pd.get_store(self.fname) as store:
            table = store['{}/gaze_samples'.format(self.group[1:])]

    def get_segment_data_subset(self, query = ''):
        with pd.get_store(self.fname) as store:
            table = store['{}/gaze_samples'.format(self.group[1:])]
        if query:
            table = table.query(query)
        return table
    
    def get_events_in_segment(self, idx, event='saccades'):
        tabrow = self.get_segment_data_subset(query='index=={}'.format(idx) ) 
        tabname = event+'_from_evt_file'
        with pd.get_store(self.fname) as store:
            table = store['{}/{}'.format(self.group[1:],tabname)]
        return table.query('{} < start_tstamp < {}'.format(
            tabrow.start_trial_tstamp.values[0], 
             tabrow.stop_trial_tstamp.values[0]) )


    def get_epochs(self, times, twin, datacols):
        with pd.get_store(self.fname) as store:
            table = store['{}/gaze_samples'.format(self.group[1:])]

        # start-times and end-times
        tstarts = times + twin[0]
        tends   = times + twin[1]
        # 'C' is a massive boolean array, with row filters for every epoch
        tt = table.time.values[:, np.newaxis]
        C = ((tt >= tstarts) & (tt < tends))
        # check for incomplete epochs (i.e. less samples than the rest)
        counts = np.sum(C, axis=0)
        faulty_eps = np.where(counts != counts.max())[0]
        # exclude faulty eps for now:
        C[:,faulty_eps] = False 
        # select remaining eps:
        cx=np.any(C, axis = 1)
        neps = counts.size - faulty_eps.size
        ntimes  = counts.max()
        # now, extract epoch data from the table: (select cols and timepoints)
        epdata = table.loc[cx,datacols].values
        epochs = epdata.reshape((neps, ntimes, len(datacols)))

        # deal with incomplete  epochs (i.e. insert nans)
        for fe in faulty_eps:
            ins = np.nan * np.ones_like(epochs[0,:,:])
            epochs = np.insert(epochs, fe, ins, axis=0) # add nans

        # diminfo: eps  x times x vars
        diminfo = [ np.arange(epochs.shape[0]),
                    np.linspace(*twin, num=epochs.shape[1]+1)[:-1],
                    datacols]
        return epochs, diminfo

if __name__ == '__main__':
    import glob
    fnames = glob.glob('../data_pp/*/*.h5')
    EDQ = EyeDataQuery
    X  = EDQ('./merged.h5', group='s00wk')
    X.get_epochs(times = [], twin = (-1000, 2000), datacols=['R_pupil_i'])